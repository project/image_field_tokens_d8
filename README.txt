INTRODUCTION
------------

The ImageField Tokens D8 module extends the default functionality
of Image Widget and Formatter and adding the ability
to specify default values and use content entity tokens in the
Alt and Title text.

INSTALLATION
-------------

1. Place the entire image_field_tokens_d8 directory into your Drupal
   modules directory.

2. Enable the image_field_tokens_d8 module by navigate to admin/modules

REQUIREMENTS
-------------

This module requires the following modules:

- Image (included in Drupal 8 Core)
- Token (https://www.drupal.org/project/token)


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.
User can configure each "Image Field Tokens" widget at entity/node display form.
